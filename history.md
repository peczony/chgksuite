# What’s new

## v0.23.0
- Для экспорта в Word:
  - добавлен режим `screen_mode=add_versions_columns`, в нём версии для ведущего и экрана расположены в двух колонках
  - добавлен флаг `--font_face`, который позволяет выбрать шрифт
  - неразрывные пробелы теперь расставляются не только в вопросах, но и в метаинформации
- Добавлена опция `--list_name`, которая позволяет при загрузке вопросов в Трелло выбрать не первый список, а любой по имени.
- Опция `--add_ts` теперь вынесена в общие и работает не только для экспорта из `.4s`, но и для импорта.
- Добавлена возможность создать файл `settings.toml` в папке `.chgksuite` и переопределить там дефолтные  настройки.
- В этом же `settings.toml` можно указать `stop_if_no_stats = true`, и тогда вы не сможете выложить в телеграм пакет без статистики взятий.

## v0.22.3
- Исправлен баг, съедавший символы подчёркивания при выкладке в телеграм.
- Добавлен флаг `inline` для поддержки вставки картинок прямо внутрь строчки (работает только в Word). Автор — Роман Сорокин.

## v0.22.2
- Пофикшен баг в предыдущей версии, мешавший получить токен.

## v0.22.1
- Сообщения в телеграме теперь отправляются без нотификации
- Пофикшен неработавший пропуск вопросов при выкладке в телеграм
- Добавлена возможность получить токен Трелло, не открывая браузер, с помощью флага `--no-browser`: `chgksuite trello token --no-browser`

## v0.22.0

- Графический интерфейс вынесен из основного пакета `chgksuite`, теперь он в пакетах `chgksuite-qt` (новый) и `chgksuite-tk` (старый). Сам интерфейс теперь вызывается командами `chgkq` (для `chgksuite-qt`) и `chgkt` (для `chgksuite-tk`).
- Удалён режим парсинга `mammoth_bs_prettify` (он не использовался по умолчанию и скорее всего это удаление никого не заденет).
- Много улучшений в экспорте для опенквиза. Например, поле «Зачёт» автоматически распределяется по нескольким строчкам, чтобы лучше работал автозачёт на Опенквизе — но в случае, если в ответе есть запятая, надо будет руками поправить обратно.
- Автоматическая расстановка неразрывных пробелов в Ворд-экспорте распространена и на поле «Ответ» (раньше была только в вопросе и комментарии).

## v0.21.0

- Ваще зря я в предыдущей версии не сделал 21, там же новые фичи были, исправляюсь :)
- Исправлен баг, при котором при наличии бэктика (\`) в тексте вопроса, в телеграме ломались спойлеры.
- Исправлен баг, при котором в телеграме не распознавались картинки в источниках.
- Исправлен баг, при котором некоторые нумерованные списки парсились как «1. 1. 1.» вместо «1. 2. 3.».

## v0.20.2

- Добавлена возможность добавить статистику взятых из Excel-файлов с турнирного сайта.
- Добавлена опция `--preserve_formatting`, сохраняющая форматирование при парсинге (полужирный и курсив).
- «Умное» разворачивание ссылок: теперь инлайн-ссылка вида `Кнут, Дональд — Википедия` не просто пропадёт после парсинга, а превратится в `Кнут, Дональд — Википедия (https://ru.wikipedia.org/wiki/Кнут,_Дональд_Эрвин)`

## v0.20.1

- Улучшен парсинг туров с нестандартной нумерацией.
- Исправлен баг, при котором у длинных вопросов часть, которая не вместилась в пост и выкладывается в комментарии, выглядела _странно_.
- Исправлен баг, при котором в телеграме не работал курсив.

## v0.20.0

- Добавлена команда `(LINEBREAK)`, которая позволяет перенести строчку внутри 4s-файла.
- Добавлена настройка, позволяющая отключить расстановку номеров вопросов при генерации презентаций.
- Добавлена возможность при загрузке из Трелло скачать не все списки, а только некоторые.
- Исправлен баг с невозможностью распарсить пакет на тарашкевице.
- Исправлен баг с тем, что на слайдах с раздатками в pptx не работало форматирование.

## v0.19.0

- Добавлены возможности форматирования: полужирный, подчёркивание, зачёркивание.
- Добавлена возможность автонумерации туров.
- Теперь, если запустить программу в интерфейсном режиме, а не консольном, она не будет закрываться после выполненного действия.

## v0.18.0

- Добавлен экспорт в OpenQuiz.
- Добавлены казахский и сербский языки.
- Улучшена работа исправлятора кривых ссылок из нового редактора Трелло.
- Добавлена возможность предупреждать пользователя, если он пытается выложить пакет без статистики взятий.
- Добавлена возможность парсить csv иного формата, нежели у турнирного сайта.
- Масштабный рефакторинг кода внутри composer.

## v0.17.4

- Ещё фиксы в регулярных выражениях для беларусского языка.
- Добавлен отдельный вариант беларусского — «by_tar» (тарашкевица).
- Улучшен алгоритм перестановки скобок в раздаточном материале. Как правильно ставить скобки: «[Раздаточный материал: текст]». Как неправильно: «Раздаточный материал: [текст]». Это имеет значение, потому что в версиях для экрана и презентации всё внутри скобок удаляется, если текст в скобках не начинается со слов «Раздаточный материал». Короче, chgksuite попробует сделать всё за вас, но вы лучше всё равно оформляйте правильно.

## v0.17.3

- Небольшой фикс в регулярных выражениях для беларусского языка.

## v0.17.2

- **Трелло**: улучшено поведение кода, который исправляет ошибки в ссылках за новым редактором.
- **Телеграм**: добавлен флаг `--reset_api` возможность сбросить `api_id` и `api_hash`, если они были введены неправильно.

## v0.17.1

- Пофикшен баг, когда chgksuite падал, если пользователь завершил сессию chgksuite в клиенте телеграма
- Теперь ссылки вида https://web.archive.org/web/20040325191708/http://www.example.com/something.html парсятся как одна ссылка, а не как две.

## v0.17.0

- Теперь в docx-файлах, которые создаёт chgksuite, гиперссылки кликабельны.
- Улучшено поведение кода, который исправляет ошибки за новым редактором трелло.

## v0.16.0

- Добавлен тег `screen`, позволяющий выводить разные версии вопроса для ведущего и для экрана (при экспорте в телеграм, презентацию, режим для экрана в Word).
- Улучшена обработка кавычек.
- Включён по умолчанию обработчик нового редактора Трелло.
- Добавлена возможность добавлять статистику не только с турнирного сайта, но и из любого csv-файла в формате турнирного сайта.
- Добавлено автоматическое проставление неразрывных дефисов.
- При экспорте в телеграм теперь вписывается `см. изображение` в случае, когда картинка в другом посте.
- Добавлена возможность эскейпинга квадратных скобок, чтобы они не удалялись в режиме для ведущего: `\[вот так\]`.

## v0.15.2

- Добавлен режим спойлеров `dots`, отбивающий спойлеры точками.
- Обновлена документация.

## v0.15.1

- Добавлено автоматическое исправление неверно поставленной квадратной скобки после раздаточного материала (`Раздаточный материал: [блабла]` → `[Раздаточный материал: блабла]`)

## v0.15.0

- Добавлена настройка `--screen_mode` при экспорте в Ворд. Теперь можно сделать версию для вывода на экран, в которой, как и в Паверпойнте, удаляются знаки ударения и указания ведущему в квадратных скобках. У настройки есть два режима,`screen_mode=replace_all` создаёт файл, где все вопросы выведены как для экрана, а `screen_mode=add_versions` создаёт файл, где каждый вопрос выведен дважды — один раз в версии для ведущего с ударениями и указаниями, а второй раз в версии для экрана без них.
- Улучшено определение ударений, введённых капсом.
- Изменены настройки по умолчанию:
  - теперь по умолчанию в имя файла не добавляется временная отметка.
  - теперь по умолчанию в имя файла картинки, извлечённой из пакета при парсинге, добавляется имя файла пакета, откуда она взята — для файла `paket.docx` картинки теперь называются не `001.png`, а `paket_001.png`.
  - теперь по умолчанию при экспорте в Ворд ответы не закрываются спойлером.
- Добавлена возможность в режиме `add_stats` подтягивать статистику не с турнирного сайта, а из файла CSV в формате турнирного сайта.
- Исправлен баг в режиме `add_stats` — теперь не сбрасывается простановка счётчика номера вопросов (типа `№№ 46`).

## v0.14.1

- Исправлен баг, при котором мог падать экспорт в телеграм.

## v0.14.0

- Теперь корректно обрабатываются случаи, когда нумерация вопросов в пакете сделана через автонумерацию Ворда.
- Чуть лучше парсятся раздаточные материалы.

## v0.13.0

- Добавлена обработка некорректных способов задания кириллических ударений (в том числе при помощи латинских букв).
- Поправлен баг, при котором не работало отключение отдельных типографских фич.

## v0.12.1

- Фикс предыдущей версии, которая могла в некоторых случаях падать при парсинге пакета.

## v0.12.0

- Теперь корректно парсятся пакеты, у которых номера вопросов отбиты просто числами, как тут: https://gitlab.com/peczony/chgksuite/-/blob/main/tests/single_number_line_test.docx

## v0.11.0

- В 4s-тексте теперь можно написать `(PAGEBREAK)` и при экспорте в docx страница будет перенесена. Это полезно для случаев, когда вы даёте кому-то на тест версию для чтения, и у вопроса есть несколько вариантов, чтобы тестеры не увидели вторую версию вопроса раньше времени.
- Списки из одного элемента больше не оформляются как списки.

## v0.10.2

- Исправлен баг, при котором код добавления статистики взятий мог падать на комментариях к дуплетам и блицам.

## v0.10.1

- **Типографика**: добавлена настройка, которая позволяет в режиме презентации не удалять знаки ударения.
- **Интернационализация**: улучшены регулярки для английского и узбекского языков.

## v0.10.0

- **Типографика**: некоторые настройки типографики (особая обработка при парсинге кавычек, тире и т.д.) теперь можно отключить.
- **Парсинг**: теперь не пропускаются элементы типа `#` (мета-комментарии), которые идут до элемента `###` (заголовок турнира).
- **Трелло**: добавлен флаг `--fix_trello_new_editor`/`-ftne`, который пытается удалить разметку, которую новый редактор Трелло без спроса заносит в карточки.

## v0.9.1

- **Трелло**: исправлен баг режима `--singlefile` («Склеить всё в один файл»), раньше там перепутывался порядок карточек, теперь не перепутывается. Также задокументировал некоторые флаги для скачивания из Трелло, которые ранее не были задокументированы.

## v0.9.0
- **Интернационализация**: теперь язык можно выбирать прямо из интерфейса. Добавлены беларусский, английский, узбекский (латиница и кириллица) языки.
- **Телеграм**: исправлен баг: теперь длинные комментарии и картинки в ответах не теряются в чате, а прикрепляются реплаем к соответствующему вопросу.
- **Паверпойнт**: теперь не только картинки, но и текстовые раздаточные материалы оформляются отдельным слайдом перед текстом вопроса.

## v0.8.8
Перешёл на новое api Pyrogram.  
Мелкие багфиксы.

## v0.8.7
Добавлен экспериментальный экспорт в телеграм.
Добавлена команда add_stats, чтобы добавить в комментарии статистику взятий.
Парсинг docx теперь работает на pypandoc, а не на PyDocX, который давно не обновляется.
Релизы 0.8.5–0.8.6 пропущен из-за бага.

## v0.8.4
В полях «Ответ» и «Зачёт» в pptx-экспорте теперь не убирается содержимое в квадратных скобках.  
Добавлена возможность сделать спойлеры не при помощи забеления, а при помощи переноса страницы (актуально, например, для тестирования с мобильных устройств).  
chgksuite больше не ломается, если недоступен модуль tkinter (например, при запуске на сервере без десктопа).  

## v0.8.3

Добавлены ретраи при обращении к API Imgur: оно в последнее время ведёт себя нестабильно.  
Добавлена возможность вписать в поле img ссылку на картинку, если вы предпочитаете не Imgur, а другой хостинг картинок (например хостинг базы). Эта возможность работает для экспортов в ЖЖ, Базу и Реддит, но не работает для экспортов в Ворд и латех.  
Теперь ссылки на загружаемые на Imgur картинки кешируются локально и chgksuite не пытается загрузить их снова, если вы снова запустите экспорт с той же картинкой.

При экспорте в Базу добавлена опция заменить знаки ударения на заглАвные бУквы, чтобы не ломать поиск Базы.

## v0.8.2

Исправлено падение под Windows из-за несовпадения дефолтной кодировки (теперь все файлы читаются в utf-8).

## v0.8.1

Исправлена пара багов предыдущей версии, в том числе баг, из-за которого не публиковалась навигация в ЖЖ.

## v0.8.0

Сильно улучшено создание презентаций, теперь картинки расставляются сами, лучше определяется размер шрифта в зависимости от длины текста. Презентации после создания готовы к употреблению и не требуют дополнительного редактирования (но это не точно). Для презентаций можно задать свой шаблон оформления.

Добавлена поддержка мультиязычности: теперь можно подложить свои файлы конфигурации с другими названиями полей не только для парсинга в формат 4s, но и для экспорта в docx/ЖЖ. Для примера поддержан украинский язык.

Локальное переопределение названий полей: если в данном конкретном вопросе какое-то поле хочется переименовать (например, «Вопрос» в «Загадка» или «Автор» в «Авторка»), можно написать в 4s что-то типа `? !!Загадка Один журналист отмечает...` или `@ !!Авторка Мария Подрядчикова`, и эти названия сохранятся в выходном файле. Слово «авторка» при парсинге автоматически сохраняется в качестве локального переопределения.

Парсер научился сохранять в 4s номер вопроса. По умолчанию сохраняется только нулевой вопрос, это поведение можно изменить (сохранять все номера или не сохранять никакие).

При экспорте в docx, pptx и ЖЖ возле предлогов и союзов теперь в правильных местах ставятся неразрывные пробелы. При экспорте в docx теперь есть номера страниц, а туры начинаются с новой страницы.

## v0.7.0

Добавлено экспериментальное создание презентаций для онлайн-игр. Выберите pptx в меню форматов экспорта. Пока что генерируемые презентации не готовы к употреблению, их нужно редактировать руками (расставлять картинки, обрабатывать дополнительно дуплеты и т.д.)

Улучшена обработка групп друзей при выкладке в ЖЖ.

Теперь тексты вопросов и комментариев при экспорте в docx не разбиваются разрывом страницы.

## v0.6.3
Теперь поправил аналогичный баг, который не давал композить файлы в другой папке, если они содержали изображения. ([коммит](https://gitlab.com/peczony/chgksuite/-/commit/5abe61cbb10e35f4b30bf216e2099d262fc3357f))

Исправил баг, при котором падал экспорт в базу, если поле «Незачёт» содержало список. ([коммит](https://gitlab.com/peczony/chgksuite/-/commit/1e0a27f0a63baf87749783944a4fb69ebd083bff))

Спасибо Рузелю Халиуллину и Станиславу Утикееву за багрепорты.

## v0.6.2

Исправил баг, который присутствовал с 0.6.0 — распарсить можно было только файл, который лежит в той же папке, что и chgksuite. ([коммит](https://gitlab.com/peczony/chgksuite/-/commit/36486f32ba36077e9645e6a2b3def7593374bf8b))

## v0.6.1

Из-за бага всё, связанное с трелло, не работало. Пофиксил. ([коммит](https://gitlab.com/peczony/chgksuite/-/commit/9aaf6ffbcd478187ed103dadb3ffe17dc63f449c))

На системах, где не установлен тех, падал тест. Это глупо. Распилил тест на два, тот, что связан с техом, по умолчанию пропускается, если есть желание его протестировать, можно запустить с ключом `--tex`. ([коммит](https://gitlab.com/peczony/chgksuite/-/commit/0a8817b9b6deea17385def5a62d905e9c5a31ad2))

Теперь можно при картинках явно указывать, в каких единицах заданы ширина и высота. ([коммит](https://gitlab.com/peczony/chgksuite/-/commit/288a1a6e24036238481a0813d6c808f12d5d1a83))

## v0.6.0

Теперь chgksuite есть на PYPI: https://pypi.org/project/chgksuite

chgksuite научилась добавлять навигацию по турам в ЖЖ-посты.

Исправлен баг в логировании.

Теперь в интерфейсе и в консоли выводится версия программы.

## v0.5.8

При выкладывании в Базу теперь происходит автоматический ресайз картинок, ограничивая максимальную сторону 600 пикселями — это решает распространённую проблему, когда картинка хорошего качества в Базе вылезала за границы экрана.

Поправил баг, когда при парсинге 4s могли создаваться списки из одного элемента.

Между вопросами в вордовском экспорте теперь по умолчанию два переноса строки, а не один. Это можно отключить.

## v0.5.7

Дофиксил предыдущий баг — были проблемы в работе с Трелло.  
В вордовском темплейте изменил дефолтный шрифт с Камбрии на Ариал.  
Под Маком теперь собираю системным питоном, а не анакондовским — иногда могут быть не видны надписи на кнопках и элементах интерфейса (говорят, ресайз окна помогает), зато под Mojave не крэшится :)  
Под Виндоус не работал парсинг текстовых файлов в формате Базы, теперь работает.  
Пофиксил баг #29 — когда в заголовке тура число идёт сначала, а слово «тур» потом, разделитель туров при парсинге ставился в неправильное место.  
Прибрался по мелочи.

## v0.5.6

Пофиксил баг в последнем релизе, когда ничего не работало из-за неправильного определения пути к текущей папке в PyInstaller

## v0.5.5

Новый GUI, смысл которого в том, чтобы он был во взаимно однозначном соответствии с аргументами командной строки. Достигается это тем, что GUI автоматически генерируется из кода, описывающего CLI. Попутно это фиксит регрессию в прошлой версии, касающуюся того, что при запуске парсинга docx-файла из GUI некорректно обрабатывались ссылки.

Мелкий фикс #27 (Александр Потапенко)

## v0.5.4

Закоммитил забытый в прошлом релизе фикс #18

## v0.5.3

(#16, #19, #22) Добавлен выбор стратегии обработки ссылок при парсинге docx (`--links`). Дефолтное значение, в отличие от поведения предыдущих версий — `unwrap`. Значение `old` воспроизводит предыдущее поведение.

(#21) Улучшен вывод в Базу: идущие подряд «meta»/«Инфо» склеиваю в один, автоматически оформляю нулевые вопросы отдельными турами (в Базе нельзя иначе).

Исправлены баги #18, #25.

## v0.5.1–0.5.2

Исправлены баги в экспорте Базы (#13, #14, #15).

## v0.5

Добавлены:

- экспорт в Базу (`compose base`)
- экспорт на реддит (`compose redditmd`)

Исправлены баги в трелло-экспорте (Евгений Миротин).

## v0.4

Добавлены:

- возможность при экспорте своей игры выгрузить только ответы (`--onlyanswers`, полезно для проверки письменного свояка)
- экспериментальная недокументированная поддержка квизбола в экспорте трелло (`--qb`)
- экспериментальная недокументированная поддержка групп друзей при экспорте в ЖЖ ([коммит](https://gitlab.com/peczony/chgksuite/commit/495b154bdf4012d4164c8db48e7685eec6fe61be))

Исправлены баги в трелло- и латех-экспорте.

## v0.3rc1

Добавлены:

- логирование
- парсинг файлов в формате Базы (Аркадий Илларионов)
- поддержка работы с Trello в интерфейсе
- поддержка работы с произвольными регулярными выражениями для парсинга полей вместо заранее заведённых авторских
- поддержка python3
- файлы requirements и requirements_dev для работы с virtualenv

Исправлено множество багов, которые лень перечислять

## v0.2rc2

- Добавлена возможность парсить сразу целую папку.
- Добавлена возможность при парсинге подставлять вместо автора вопроса имя файла, если автор не указан.
- Добавлен экспорт вопросов в Trello (отдельный Python-скрипт, без поддержки в GUI и бинарных сборках).
- Автоматически определяются заголовок и дата пакета.
- Теперь в ЖЖ можно одним кликом выложить весь пакет: есть возможность автоматически разбить на туры и добавить пост про общие впечатления. К сожалению, выкладка в ЖЖ стала медленнее: добавлены паузы, чтобы соблюдать рейт-лимиты жежешного антиспама.
- Исправлен баг с дефисом-минусом методом отрывания функциональности автоматической расстановки символов «–» (короткое тире) и «−» (минус), теперь автоматически расставляются только длинные тире и кавычки. Возможно, в будущем после доработки фичу верну.
- Исправлены все остальные баги из v0.2rc1, кроме некорректного распознавания формата Базы.

## v0.2rc1

- Вместо отдельных файлов `chgk_parser` и `chgk_composer` появилась единая точка входа — `chgksuite`. Для использования в консоли вместо `chgk_parser` теперь нужно писать `chgksuite parse`, вместо `chgk_composer` — `chgksuite compose`, в остальном без изменений.
- Больше нет необходимости копировать файлы `chgksuite` в каждый каталог с файлами вопросов, вопросы и `chgksuite` могут жить отдельно.
- Размеры картинок определяются автоматически (слишком большие уменьшаются, слишком маленькие увеличиваются).
- Запоминается последний рабочий каталог.
- В обоих режимах (импорт, экспорт) появились предупреждения об ошибках вида `WARNING: question (текст вопроса) lacks the following fields: source, ...` — с перечислением отсутствующих, по мнению парсера, полей.
- Улучшена поддержка файлов с кириллическими именами под Windows (но лучше всё равно их не используйте).
- Добавлен импорт пакета из Trello (отдельный Python-скрипт, без поддержки в GUI и бинарных сборках).
- Добавлено автоматическое определение кодировки txt-файлов.
- Рабочий каталог chgksuite больше не засоряется временными файлами при экспорте в tex.
- Появились тесты.
- Исправлено множество багов (например, баг, когда при использовании бинарной сборки под Windows не выкладывались в ЖЖ пакеты с картинками)

Известные неисправленные баги:

- Файлы с символом дефиса-минуса `-` в имени или пути могут распознаваться и создаваться некорректно.
- Текстовые файлы в формате Базы Вопросов распознаются некорректно.
- Дуплеты и блицы распознаются некорректно.
- Вопросы, содержащие последовательности символов «вопрос.» и «ответ.», распознаются некорректно.