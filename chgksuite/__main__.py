#!/usr/bin/env python
# -*- coding: utf-8 -*-
from chgksuite.cli import app


def main():
    app()


if __name__ == "__main__":
    main()
